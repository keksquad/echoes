from echoes.app import create_app
from echoes.models import db

if __name__ == '__main__':
    app = create_app()
    db.bind(app.config['DB_TYPE'], app.config['DB_STRING'], create_db=False)
    db.generate_mapping()
    app.run('0.0.0.0')
