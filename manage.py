#!/usr/bin/env python

from flask_script import Manager
from echoes.models import db
from echoes.app import create_app
from pony import orm

app = create_app()
manager = Manager(app)

@manager.command
def init_db():
    orm.sql_debug(True)
    db.bind(app.config['DB_TYPE'], app.config['DB_STRING'], create_db=True)
    db.generate_mapping(create_tables=True)
    print("Database initialized")


@manager.command
def add_user():
    raise NotImplementedError


if __name__ == "__main__":
    manager.run()