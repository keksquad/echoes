#!/usr/bin/env bash

# Deploy to server
./marble/script.sh \
      --project echoes \
      --type production \
      --blobs "frontend" \
      --containers "python" \
      --destination /home/ci/lithos/images \
      --server lentach.uwtech.org \
      --port 2222 \
      --user ci