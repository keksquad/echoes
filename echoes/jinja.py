
# -*- encoding: utf-8 -*-
"""
    flask_triangle.jinja
    --------------------
    A set of features for Jinja2 to facilitate AngularJS integration in your
    templates.
    :copyright: (c) 2013 by Morgan Delahaye-Prat.
    :license: BSD, see LICENSE for more details.
"""

from __future__ import absolute_import
from __future__ import unicode_literals

from jinja2 import evalcontextfilter, Undefined, is_undefined


class TriangleUndefined(Undefined):

    def __getattr__(self, name):
        if name[1] == '_':
            raise AttributeError(name)
        return TriangleUndefined(name='{}.{}'.format(self._undefined_name,
                                                     name))


def angular_filter(value):
    """
    A filter to tell Jinja2 that a variable is for the AngularJS template
    engine.
    If the variable is undefined, its name will be used in the AngularJS
    template, otherwise, its content will be used.
    """

    if is_undefined(value):
        return '{{{{{}}}}}'.format(value._undefined_name)
    if type(value) is bool:
        value = repr(value).lower()
    return '{{{{{}}}}}'.format(value)