import os
from flask import Flask, request
from flask_restful import Api

from .views import root
from .views import uploader

from .resources import init as resources_init

from .jinja import TriangleUndefined, angular_filter
from .utils import get_static_hash


def create_app():
    app = Flask(__name__, static_folder='../static')

    environment = os.environ.get('ECHOES_CONFIG', 'config.Development')
    print('Using {}'.format(environment))
    app.config.from_object(environment)

    static_hash = get_static_hash()
    print('Using static version {}'.format(static_hash))

    app.jinja_env.undefined = TriangleUndefined
    app.jinja_env.filters['angular'] = angular_filter

    # Register app endpoints
    root.init(app)
    uploader.init(app)
    resources_init(app)

    @app.context_processor
    def set_static_hash():
        if app.config['DEBUG']:
            hash = get_static_hash()
        else:
            hash = static_hash

        return dict(static_hash=hash)

    if app.config['DEBUG']:
        from pony import orm
        from flask import send_from_directory

        orm.sql_debug(True)

        @app.route('/uploads/<path:path>')
        def uploads(path):
            return send_from_directory('../uploads', path)

    return app
