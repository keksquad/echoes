from datetime import datetime
from pony import orm

db = orm.Database()

class BlDomain(db.Entity):
    domain = orm.Required(str)

class Post(db.Entity):
    summary = orm.Required(str)
    text = orm.Optional(str)
    links = orm.Set('PostLink')
    photos = orm.Set("Photo")
    user = orm.Required("User")
    state = orm.Required(str, sql_default='pending')
    timestamp = orm.Required(datetime, sql_default='CURRENT_TIMESTAMP')

class PostLink(db.Entity):
    link = orm.Required(str)
    post = orm.Required('Post')

class Photo(db.Entity):
    user = orm.Required("User")
    url = orm.Required(str)
    post = orm.Optional("Post")
    latitude = orm.Optional(float)
    longitude = orm.Optional(float)
    timestamp = orm.Required(datetime, sql_default='CURRENT_TIMESTAMP')

class User(db.Entity):
    vk_id = orm.Required(int, unique=True)
    balance = orm.Optional(int, sql_default='0')
    photos = orm.Set("Photo")
    posts = orm.Set("Post")
