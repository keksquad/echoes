from .app import create_app
from .models import db

app = create_app()
db.bind(app.config['DB_TYPE'], app.config['DB_STRING'], create_db=False)
db.generate_mapping()
