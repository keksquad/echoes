import os
import hashlib
from pony import orm
from .models import User

def get_static_hash():
    if os.path.exists('frontend.ver'):
        fp = open('frontend.ver', "r")
    else:
        raise FileNotFoundError

    content = fp.read()
    fp.close()
    return content

def check_user(app_id, viewer_id, app_secret, hash):
    m = hashlib.md5('{}_{}_{}'.format(str(app_id), str(viewer_id), app_secret).encode())
    return m.hexdigest().lower() == hash

@orm.db_session
def get_user_from_db(vk_id):
    user = User.get(vk_id=vk_id)
    if user is None:
        user = User(vk_id=vk_id, balance=0)
    return user
