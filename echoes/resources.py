from flask import abort, request
from flask_restful import Api, Resource, fields, marshal_with
from pony import orm
from .models import BlDomain, Post, PostLink, Photo
from .utils import check_user, get_user_from_db


def init(app):
    api = Api(app)

    class BlDomainResource(Resource):
        @orm.db_session
        def get(self, id):
            domain = BlDomain.get(id=id)
            if domain is None:
                abort(404)
            else:
                return domain.to_dict()

        @orm.db_session
        def delete(self, id):
            BlDomain.get(id=id).delete()
            return '', 204

    class BlDomainListResource(Resource):
        @orm.db_session
        def get(self):
            domains = BlDomain.select()
            return [domain.to_dict() for domain in domains]

        @orm.db_session
        def post(self):
            data = request.get_json()
            BlDomain(
                domain=data.get('domain')
            )
            return data

    class PostResource(Resource):
        @orm.db_session
        def get(self, id):
            post = Post.get(id=id)
            if post is None:
                abort(404)
            else:
                return {
                    'id': post.id,
                    'summary': post.summary,
                    'text': post.text,
                    'links': [link.to_dict() for link in post.links],
                    'photos': [{
                        'id': photo.id,
                        'url': photo.url,
                        'latitude': photo.latitude,
                        'longitude': photo.longitude,
                        'timestamp': photo.timestamp.isoformat()
                    } for photo in post.photos],
                    'user': post.user.to_dict(),
                    'state': post.state,
                }

        @orm.db_session
        def delete(self, id):
            Post.get(id=id).delete()
            return '', 204

    class PostListResource(Resource):
        @orm.db_session
        def get(self):
            posts = Post.select()
            return [{
                'id': post.id,
                'summary': post.summary,
                'text': post.text,
                'links': [link.to_dict() for link in post.links],
                'photos': [{
                    'id': photo.id,
                    'url': photo.url,
                    'latitude': photo.latitude,
                    'longitude': photo.longitude,
                    'timestamp': photo.timestamp.isoformat()
                } for photo in post.photos],
                'user': post.user.to_dict(),
                'state': post.state,
            } for post in posts]

        @orm.db_session
        def post(self):
            data = request.get_json()
            app_id = app.config['APP_ID']
            app_secret = app.config['APP_SECRET']
            user_id = data.get('user_id')
            user_key = data.get('user_key')
            if not check_user(app_id, user_id, app_secret, user_key):
                abort(403)
            choices = data.get('choices', None)
            files = data.get('files', None)

            if choices is not None:
                for choice in choices:
                    domain = BlDomain.get(domain=choice['url'])
                    if domain is not None:
                        # Domain blacklisted
                        abort(403)

            post = Post(
                summary=data.get('summary'),
                text=data.get('text', ''),
                user=get_user_from_db(user_id)
            )
            orm.commit()

            if choices is not None:
                for choice in choices:
                    PostLink(link=choice['url'], post=post)
            if files is not None:
                for file in files:
                    photo = Photo[file['id']]
                    photo.post = post
            return data

    class UserPostListResource(Resource):
        @orm.db_session
        def get(self, user_id):
            posts = Post.select(lambda p: p.user.id == user_id)
            return [{
                'id': post.id,
                'summary': post.summary,
                'text': post.text,
                'links': [link.to_dict() for link in post.links],
                'photos': [{
                    'id': photo.id,
                    'url': photo.url,
                    'latitude': photo.latitude,
                    'longitude': photo.longitude,
                    'timestamp': photo.timestamp.isoformat()
                } for photo in post.photos],
                'user': post.user.to_dict(),
                'state': post.state,
            } for post in posts]

    api.add_resource(BlDomainListResource, '/api/bldomains')
    api.add_resource(BlDomainResource, '/api/bldomains/<string:id>')
    api.add_resource(PostListResource, '/api/posts')
    api.add_resource(PostResource, '/api/posts/<string:id>')
    api.add_resource(UserPostListResource, '/api/users/<string:user_id>/posts')
