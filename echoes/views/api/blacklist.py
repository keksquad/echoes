# coding=utf-8
import json
from flask import request
from pony import orm
from ...models import BlacklistedDomain


def init(app):
    @app.route('/api/blacklist', methods=['GET'])
    @orm.db_session
    def api_blacklist_get_all():
        limit = 10
        page = int(request.args.get('page', 1))
        offset = limit * (page - 1)
        return json.dumps({
            'status': 'ok',
            'data': [
                {
                    'id': d.id,
                    'domain': d.domain,
                } for d in BlacklistedDomain.select()
                    .order_by(lambda r: r.domain)
                    .limit(limit, offset=offset)
            ],
        })

    @app.route('/api/blacklist', methods=['POST'])
    @orm.db_session
    def api_blacklist_post():
        pattern = request.get_json().get('domain')
        if pattern is None:
            return json.dumps({
                'status': 'error',
                'error': {
                    'type': 'required_parameter_not_present',
                    'message': 'parameter ‘domain’ is required',
                    'required_parameter': 'domain',
                },
            })
        domain = BlacklistedDomain(domain=pattern)
        return json.dumps({
            'status': 'ok',
            'data': {
                'id': domain.id,
                'domain': domain.domain,
            },
        })

    @app.route('/api/blacklist/<int:id>', methods=['GET'])
    @orm.db_session
    def api_blacklist_get(id):
        for blacklisted_domain in BlacklistedDomain.select(lambda p: p.id == id):
            return json.dumps({
                'status': 'ok',
                'data': {
                    'id': blacklisted_domain.id,
                    'domain': blacklisted_domain.domain
                },
            })
        return json.dumps({
            'status': 'error',
            'error': {
                'type': 'object_not_found',
                'message': 'domain with specified identifier not found',
                'erroneous_id': id,
            },
        })

    @app.route('/api/blacklist/<int:id>', methods=['PATCH'])
    @orm.db_session
    def api_blacklist_patch(id):
        new_domain = request.get_json().get('domain')
        if new_domain is None:
            return json.dumps({
                'status': 'error',
                'error': {
                    'type': 'required_parameter_not_present',
                    'message': 'parameter ‘domain’ is required',
                    'required_parameter': 'domain',
                }
            })
        domain = BlacklistedDomain[id]
        domain.domain = new_domain
        return json.dumps({
            'status': 'ok',
        })

    @app.route('/api/blacklist/<int:id>', methods=['DELETE'])
    @orm.db_session
    def api_blacklist_delete(id):
        try:
            BlacklistedDomain[id].delete()
            return json.dumps({
                'status': 'ok',
            })
        except orm.core.ObjectNotFound:
            return json.dumps({
                'status': 'error',
                'error': {
                    'type': 'object_not_found',
                    'message': 'domain with specified identifier not found',
                    'erroneous_id': id,
                }
            })
