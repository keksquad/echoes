# coding=utf-8

import json
import datetime
from flask import request
from pony import orm
from ..root import check_user
from ...models import Post, PostLink, Photo
from ...utils import get_user_from_db

date_handler = lambda obj: (
    obj.isoformat()
    if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date)
    else None
)

def init(app):
    @app.route('/api/posts', methods=['GET'])
    @orm.db_session
    def api_posts_get_all():
        limit = 10
        page = int(request.args.get('page', 1))
        offset = limit * (page - 1)
        return json.dumps({
            'status': 'ok',
            'data': [
                {
                    'id': d.id,
                    'domain': d.domain,
                } for d in Post.select()
                    .order_by(lambda r: orm.desc(r.timestamp))
                    .limit(limit, offset=offset)
            ],
        })

    @app.route('/api/posts', methods=['POST'])
    @orm.db_session
    def api_posts_post():
        request_json = request.get_json()
        app_id = app.config['APP_ID']
        app_secret = app.config['APP_SECRET']
        user_id = request_json.get('user_id', None)
        user_key = request_json.get('user_key', None)
        links = request_json.get('links', None)
        files = request_json.get('files', None)

        if not check_user(app_id, user_id, app_secret, user_key):
            return json.dumps({
                'error': True,
                'message': 'Please provide VK user credentials'
            })
        user = get_user_from_db(user_id)
        post = Post(summary=request_json.get('summary', None),
                    text=request_json.get('text', ''),
                    user=user,
                    state='pending')
        for link in links:
            if link is not '':
                # TODO check for the blacklist
                lnk = PostLink(link=link, post=post)

        for file in files:
            Photo[file['id']].post = post

        orm.commit()
        return json.dumps({
            'error': False,
            'message': 'Ваша новость была успешно отправлена',
            'data': {
                'id': post.id,
            },
        })

    @app.route('/api/posts/<int:id>', methods=['GET'])
    @orm.db_session
    def api_posts_get(id):
        try:
            post = Post[id]

            return json.dumps({
                'error': False,
                'data': post.to_dict(),
                'links': [link for link in post.links],
                'photos': [{
                    'id': photo.id,
                    'url': photo.url
                } for photo in post.photos]
            }, default=date_handler)

        except orm.core.ObjectNotFound:
            return json.dumps({
                'error': True,
                'message': 'Post with specified identifier not found'
            })

    @app.route('/api/posts/<int:id>', methods=['DELETE'])
    @orm.db_session
    def api_posts_delete(id):
        try:
            Post[id].delete()
            return json.dumps({
                'error': False,
            })
        except orm.core.ObjectNotFound:
            return json.dumps({
                'error': True,
                'message': 'Post with specified identifier not found'
            })
