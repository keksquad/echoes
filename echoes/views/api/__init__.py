from .blacklist import init as blacklist
from .posts import init as posts
from .topics import init as topics
from .users import init as users

def init(app):
    blacklist(app)
    posts(app)
    topics(app)
    users(app)
