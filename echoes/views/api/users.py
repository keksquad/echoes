# coding=utf-8

import json


def init(app):
    @app.route('/api/users/', methods=['GET'])
    def api_users_get_all():
        # TODO: Fetch data from database
        # TODO: Paginate
        return json.dumps({
            'status': 'ok',
            'data': [
                {
                    'id': 12721671,
                    'viewer_type': 1,
                    'rating': 228,
                    'balance': 999.99,
                    'banned': False,
                    'posts_count': 17,
                },
                {
                    'id': 81250512,
                    'viewer_type': 3,
                    'rating': 13204,
                    'balance': 52.35,
                    'banned': False,
                    'posts_count': 1,
                },
            ],
        })

    @app.route('/api/users/<int:id>', methods=['GET'])
    def api_users_get(id):
        # TODO: Obtain data from database
        return json.dumps({
            'status': 'ok',
            'data': {
                'id': 12721671,
                'viewer_type': 1,
                'rating': 228,
                'balance': 999.99,
                'banned': False,
                'posts_count': 17,
            },
        })

    @app.route('/api/users/<int:id>/posts', methods=['GET'])
    def api_users_posts_get(id):
        # TODO: Obtain data from database
        return json.dumps({
            'status': 'ok',
            'data': [
                {
                    'id': 1,
                    'summary': 'Навального вышвырнули',
                    'details': 'Жёстко! Смотреть без регистрации и СМС.',
                    'source': None,
                    'photo': '25.jpg',
                    'video': None,
                    'location': {
                        'latitude': 228.48,
                        'longitude': 32.1488,
                    },
                    'author': id,
                    'state': 'approved',
                },
                {
                    'id': 1,
                    'summary': 'Навальный не допущен до выборов.',
                    'details': 'Смотреть на это собрались все кремлеботы.',
                    'source': 'http://news.ru/post/12152',
                    'photo': '73.jpg',
                    'video': '2228.mpg',
                    'location': {
                        'latitude': 218.78,
                        'longitude': 36.1488,
                    },
                    'author': id,
                    'state': 'published',
                },
            ],
        })
