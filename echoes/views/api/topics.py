# coding=utf-8

import json


def init(app):
    @app.route('/api/topics/', methods=['GET'])
    def api_topics_get_all():
        # TODO: Fetch data from database
        # TODO: Paginate
        return json.dumps({
            'status': 'ok',
            'data': [
                {
                    'id': 1,
                    'title': 'Навального вышвырнули с выборов!',
                    'count': 3,
                },
                {
                    'id': 2,
                    'title': 'Мемы запретили в России',
                    'count': 12,
                },
            ],
        })

    @app.route('/api/topics/<int:id>', methods=['GET'])
    def api_topics_get(id):
        # TODO: Obtain data from database
        return json.dumps({
            'status': 'ok',
            'data': {
                'id': 1,
                'title': 'Навального вышвырнули с выборов!',
                'count': 3,
            },
        })

    @app.route('/api/topics/<int:id>/posts', methods=['GET'])
    def api_topics_posts_get(id):
        # TODO: Obtain data from database
        return json.dumps({
            'status': 'ok',
            'data': [
                {
                    'id': 1,
                    'summary': 'Навального вышвырнули',
                    'details': 'Жёстко! Смотреть без регистрации и СМС.',
                    'source': None,
                    'photo': '25.jpg',
                    'video': None,
                    'location': {
                        'latitude': 228.48,
                        'longitude': 32.1488,
                    },
                    'author': 217612706,
                    'state': 'approved',
                },
                {
                    'id': 1,
                    'summary': 'Навальный не допущен до выборов.',
                    'details': 'Смотреть на это собрались все кремлеботы.',
                    'source': 'http://news.ru/post/12152',
                    'photo': '73.jpg',
                    'video': '2228.mpg',
                    'location': {
                        'latitude': 218.78,
                        'longitude': 36.1488,
                    },
                    'author': 70127420,
                    'state': 'published',
                },
            ],
        })
