from flask import render_template, request
from ..utils import check_user, get_user_from_db
import json
import uuid
from ..models import User, Photo
from pony import orm
from flask_uploads import configure_uploads, UploadSet, UploadNotAllowed, IMAGES

def init(app):
    photos = UploadSet('photos', IMAGES)
    configure_uploads(app, (photos))

    @app.route('/upload/<int:id>/<string:key>', methods=['GET', 'POST'])
    def upload(id, key):
        if check_user(app.config['APP_ID'], id, app.config['APP_SECRET'], key) is False:
            return render_template('error.html')

        if request.method == 'POST' and 'file' in request.files:
            try:
                filename = photos.save(request.files['file'], name='{}.'.format(uuid.uuid4()))
                with orm.db_session:
                    user = get_user_from_db(id)
                    # TODO(spark): parse exif for location
                    photo = Photo(url=photos.url(filename), user=user)
                    orm.commit()
                    return json.dumps({'id': photo.id,'filename': photos.url(filename)})
            except UploadNotAllowed:
                return json.dumps({'error':True})
        return json.dumps({'error':True})