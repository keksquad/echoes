from flask import render_template, request, redirect, url_for, g
from ..utils import check_user, get_user_from_db
from ..models import User, Post
from vklancer import api
from pony import orm

def init(app):
    @app.route('/')
    def index():
        viewer_id = request.args.get('viewer_id')
        viewer_type = request.args.get('viewer_type')
        group_id = request.args.get('group_id')
        auth_key = request.args.get('auth_key')
        if viewer_id is None or viewer_type is None or group_id is None or auth_key is None or check_user(app.config['APP_ID'], viewer_id, app.config['APP_SECRET'], auth_key) is False:
            return render_template('error.html')

        get_user_from_db(viewer_id)

        if viewer_type == '4': # Admin
            return redirect(url_for('admin', _scheme='https', _external=True, id=viewer_id, key=auth_key))
        elif viewer_type == '3': # Editor
            return redirect(url_for('editor', _scheme='https', _external=True, id=viewer_id, key=auth_key))
        else:
            return redirect(url_for('submit', _scheme='https', _external=True, id=viewer_id, key=auth_key))

    @app.route('/submit/<int:id>/<string:key>')
    def submit(id, key):
        if check_user(app.config['APP_ID'], id, app.config['APP_SECRET'], key) is False:
            return render_template('error.html')
        vk = api.API()
        user = vk.users.get(user_ids=id, fields='has_photo,photo_50')
        usr = get_user_from_db(id)
        return render_template('submit.html', id=id, key=key, user=user['response'][0], user_balance=usr.balance)

    @app.route('/admin/<int:id>/<string:key>')
    def admin(id, key):
        if check_user(app.config['APP_ID'], id, app.config['APP_SECRET'], key) is False:
            return render_template('error.html')
        return render_template('admin.html', id=id, key=key)

    @app.route('/editor/<int:id>/<string:key>')
    def editor(id, key):
        if check_user(app.config['APP_ID'], id, app.config['APP_SECRET'], key) is False:
            return render_template('error.html')
        return render_template('editor.html', id=id, key=key)

    @app.route('/api/banuser/<int:id>')
    @orm.db_session
    def banuser(id):
        post = Post.get(id=id)
        vk_id = post.user.vk_id

        vk = api.API(token=app.config['GROUP_SECRET'])
        result = vk.groups.banUser(group_id=155369320, user_id=vk_id)
        print(result)
        return ''

    if app.config['DEBUG']:
        @app.route('/submit')
        def submit_dev():
            vk = api.API()
            user = vk.users.get(user_ids=1, fields='has_photo,photo_50')
            usr = get_user_from_db(1)
            return render_template('submit.html', id=1, key='d666f7eddb8aa15092c48c64bff6c7d4', user=user['response'][0], user_balance=usr.balance)

        @app.route('/admin')
        def admin_dev():
            return render_template('admin.html', id=1, key='d666f7eddb8aa15092c48c64bff6c7d4')

        @app.route('/editor')
        def editor_dev():
            return render_template('editor.html', id=1, key='d666f7eddb8aa15092c48c64bff6c7d4')
