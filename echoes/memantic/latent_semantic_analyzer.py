# coding: utf-8
import math

from porter_stemmer import PorterStemmer

import string
from normalizer import normalize
from numpy import zeros, sum, asarray
from numpy.linalg import svd

WHITESPACE = ' ' * 35

DIMENSIONS = 100


class LatentSemanticAnalyser:
    def __init__(self, stop_words, ignored_characters, documents):
        # Все слова из корпуса со ссылками на документы, где они встречаются
        self.known_words = {}

        # Все проиндексированные слова
        self.dictionary = []

        # Список шумовых слов, которые не включаются в индекс
        self.stop_words = stop_words

        # TODO: Опиши меня
        self.keys = []

        # Инициализируем правила нормализации
        # TODO: Использовать коллекцию вместо строки
        self.ignored_characters = ignored_characters

        # Инициализируем документы
        self.documents = []
        for document in documents:
            self.add_document(document)

        self.stamps = {
            'documents': 0,
            'matrix': 0,
            'tf_idf': 0,
            'svd': 0,
        }

    def prepare(self):
        self.build_document_term_matrix()
        # TODO: Выполнять или нет нормализацию TF-IDF?
        # Следует изучить влияние нормализации на обработку больших корпусов.
        self.perform_tf_idf_normalization()
        self.perform_singular_value_decomposition()

    def find_word_index(self, word, add=False):
        word = PorterStemmer.stem(word)
        if word in self.dictionary:
            return self.dictionary.index(word)
        else:
            if add:
                self.dictionary.append(word)
                return len(self.dictionary) - 1
            else:
                return None

    def add_document(self, text):
        words = normalize(text).lower().split()
        self.documents.append([])
        document_index = len(self.documents) - 1
        for word in words:
            if word in self.stop_words:
                continue
            word_index = self.find_word_index(word, True)
            self.documents[document_index].append(word_index)
            if word_index in self.known_words:
                self.known_words[word_index].append(document_index)
            else:
                self.known_words[word_index] = [document_index]
        self.stamps['documents'] += 1

    def build_document_term_matrix(self):
        if self.stamps['matrix'] >= self.stamps['documents']:
            return

        print("* Создаётся матрица «терм-на-документ»… ", end='', flush=True)
        self.keys = [k for k in self.known_words.keys() if len(self.known_words[k]) > 0]
        self.keys.sort()
        self.document_term_matrix = zeros([len(self.keys), len(self.documents)])
        for i, k in enumerate(self.keys):
            message = "\r* Создаётся матрица «терм-на-документ»… обрабатывается терм {:>4} из {:>4}".format(i, len(
                self.keys))
            print(message, WHITESPACE, "\r", end='', flush=True)
            for d in self.known_words[k]:
                self.document_term_matrix[i, d] += 1

        self.stamps['matrix'] = self.stamps['documents']
        print("\r* Создаётся матрица «терм-на-документ»… готово.", WHITESPACE, "\r", flush=True)

    def perform_singular_value_decomposition(self):
        self.perform_tf_idf_normalization()
        if self.stamps['svd'] >= self.stamps['tf_idf']:
            return
        print("* Выполняется сингулярное разложение.")
        print("  Этот процесс может занять некоторое время.")
        print("  Пожалуйста, подождите… ", end='', flush=True)
        U, S, Vt = svd(self.document_term_matrix)
        self.unitary_matrix = U
        self.singularities = S
        self.transposed_unitary_matrix = Vt
        self.stamps['svd'] = self.stamps['tf_idf']
        print("готово.")

    def perform_tf_idf_normalization(self):
        self.build_document_term_matrix()
        if self.stamps['tf_idf'] >= self.stamps['matrix']:
            return
        print("* Выполняется TF-IDF нормализация… \r", end='', flush=True)
        words_per_doc = sum(self.document_term_matrix, axis=0)
        docs_per_word = sum(asarray(self.document_term_matrix > 0, 'i'), axis=1)
        rows, columns = self.document_term_matrix.shape
        for i in range(rows):
            if i % 10 == 0:
                print("* Выполняется TF-IDF нормализация… "
                      "обрабатывается терм", i + 1, 'из', rows,
                      WHITESPACE, "\r",
                      end='', flush=True)
            self.document_term_matrix[i] = [
                self.document_term_matrix[i, j] / words_per_doc[j] *
                math.log10(float(columns) / docs_per_word[i])
                for j in range(columns)
            ]
        print("* Выполняется TF-IDF нормализация… готово.", WHITESPACE, flush=True)
        self.stamps['tf_idf'] = self.stamps['matrix']

    def dump_document_term_matrix(self):
        print('Матрица термы-на-документы:')
        print()
        for i, row in enumerate(self.document_term_matrix):
            print('{:>30s} : {}'.format(self.dictionary[i], row))

    def dump_singular_value_decomposition(self):
        print('Сингулярные значения:')
        print(self.singularities)
        print('Унитарная матрица документов: ')
        for i, row in enumerate(self.unitary_matrix):
            print(self.dictionary[self.keys[i]], row[0:DIMENSIONS + 1])
        print('Транспонированная унитарная матрица лексем:')
        print(-1 * self.transposed_unitary_matrix[0:DIMENSIONS + 1, :])

    def find(self, keyword):
        self.prepare()
        index = self.find_word_index(keyword)
        if not index:
            print('Слово не встречается в корпусе.')
            return []
        if index not in self.keys:
            print('Слово является стоп-символом и недоступно для поиска.')
            return []

        index = self.keys.index(index)
        print('Слово: «{}», нормальная форма: «{}»'.format(keyword, self.dictionary[self.keys[index]]))

        # Получаем координаты слова
        word = (-1 * self.unitary_matrix[:, 1:(DIMENSIONS + 1)])[index]
        print('Слово: «{}» (№ {}), координаты:\n{}'.format(keyword, index, word))
        results = []
        coordinates = -1 * self.transposed_unitary_matrix[1:(DIMENSIONS + 1), :]
        for i, document in enumerate(self.documents):
            vector = [coordinates[d][i] for d in range(DIMENSIONS)]
            delta = [float(word[d] - vector[d]) for d in range(DIMENSIONS)]
            distance = sum(i * i for i in delta)
            results.append({
                'number': i,
                'document': document,
                'vector': vector,
                'distance': distance
            })
        return sorted(results, key=lambda record: record['distance'])

    def get_documents(self):
        self.prepare()
        results = []
        coordinates = -1 * self.transposed_unitary_matrix[1:(DIMENSIONS + 1), :]
        for i, document in enumerate(self.documents):
            vector = [coordinates[d][i] for d in range(DIMENSIONS)]
            results.append({
                'number': i,
                'vector': vector,
            })
        return sorted(results, key=lambda record: record['number'])
