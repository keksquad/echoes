# coding=utf-8

from latent_semantic_analyzer import DIMENSIONS
from latent_semantic_analyzer import LatentSemanticAnalyser

from os.path import dirname

NUMBER_OF_RANKED_RESULTS = 10
THRESHOLD_DISTANCE = 0.097

with open(dirname(__file__) + '/documents', 'r') as f:
    texts = [line.strip() for line in f.readlines() if len(line) > 1]

with open(dirname(__file__) + '/ignored_characters', 'r') as f:
    ignored_characters = f.read()

with open(dirname(__file__) + '/stop_words', 'r') as f:
    stop_words = [line.strip() for line in f.readlines()]

print('ЛАТЕНТНЫЙ СЕМАНТИЧЕСКИЙ АНАЛИЗАТОР')
print('Текстовый интерфейс. Версия 2.0.')
print('© Дубенецкий Егор, 2017')

print()
print("ИНИЦИАЛИЗАЦИЯ МОДЕЛИ")
print('* Соединение с хранилищем корпуса')
print('  - Загрузка параметров… ', end='')
lsa = LatentSemanticAnalyser(stop_words, ignored_characters, [])
print('готово.')
print('  - Загрузка и нормализация документов… ', end='', flush=True)
for i, document in enumerate(texts):
    lsa.add_document(document)
    print('\r  - Загрузка и нормализация документов…', i, 'из', len(texts), end='', flush=True)
print('\r  - Загрузка и нормализация документов… готово.' + (' ' * 20))

print('  Корпус успешно инициализирован.')
print('* Метаданные модели:')
print('  - Шумовых слов:', len(stop_words))
print('  - Игнорируемых символов:', len(ignored_characters))
print('  - Документов:', len(texts))
print('* Топология корпуса:')
print('  - Пороговое Евклидово расстояние:', THRESHOLD_DISTANCE)
print('  - Размерность пространства документов:', DIMENSIONS, 'измерений')

print()
print("ОБРАБОТКА КОРПУСА")
lsa.prepare()
# TODO: lsa.dump_document_term_matrix()
# TODO: lsa.dump_singular_value_decomposition()

print()
print("ГОТОВО")
print('Корпус полностью загружен и готов к выполнению запросов.')
print(f'Сейчас в корпусе { len(texts) } новостей.')
print('Введите ключевое слово для поиска или добавьте новость в корпус.')

while True:
    print('> ', end='')
    word = input()
    if word.startswith('.'):
        lsa.add_document(word)
        print("Документ добавлен в корпус.")
        print("Выполняется обновление индекса…")
        lsa.build_document_term_matrix()
        lsa.perform_singular_value_decomposition()
        print("Поиск похожих новостей:")
        # todo

    elif word.startswith('*'):
        # Отображение всей кластеризации
        documents = lsa.get_documents()
        gone = [None for record in documents]
        filename = 'report.txt'
        print(f'Отчёт ‘{filename}’ оформляется…')
        with open(filename, 'w') as f:
            printed = [False for document in documents]
            for document in documents:
                if printed[document['number']]:
                    continue
                print('#' * 72, file=f)
                print(f'Новость № {document["number"]+1}:', file=f)
                print(texts[document['number']], file=f)
                for i, another_document in enumerate(documents):
                    if another_document['number'] == document['number']:
                        continue
                    distance = sum(
                        (document['vector'][i] - another_document['vector'][i]) ** 2
                        for i in range(DIMENSIONS)
                    )
                    if distance < THRESHOLD_DISTANCE:
                        printed[another_document['number']] = True
                        print('-' * 72, file=f)
                        print('Похожая новость № {} (межвекторное расстояние: {:0.5f}):'.format(
                            another_document['number'] + 1,
                            distance,
                        ), file=f)
                        print(texts[another_document['number']], file=f)
                print(file=f)
                print(file=f)
        print(f"Отчёт записан в файл ‘{filename}’. ;)")
    else:
        # Поиск документов по ключевым словам
        print(f"Выполняется поиск по запросу «{word}»…")
        search_result = lsa.find(word)
        print(f"Первые {NUMBER_OF_RANKED_RESULTS} результатов по Вашему запросу:")
        for rank, document in enumerate(search_result):
            if rank < NUMBER_OF_RANKED_RESULTS:
                print(f"РЕЗУЛЬТАТ № {rank + 1}:")
                print(documents[document['number']])
                print()

exit(0)

# Кластеризация методом усталого Егора

search_result = []  # FIXME
assignment = [None for _ in search_result]
cluster_no = 0


def expand_from(i, eps):
    record = search_result[i]
    assignment[i] = cluster_no
    for j, another_record in enumerate(search_result):
        if assignment[j]:
            continue
        distance = sum((record['vector'][i] - another_record['vector'][i]) ** 2 for i in range(100))
        if distance < eps:
            assignment[j] = cluster_no


for i, record in enumerate(search_result):
    if not assignment[i]:
        expand_from(i, THRESHOLD_DISTANCE)
        cluster_no += 1

for i, record in enumerate(search_result):
    print(assignment[i], texts[record['number']])
    print()

### Кластеризация ручками

#     gone = [None for record in search_result]
# for record in search_result:
#     print('- {:>3} ({}): {}'.format(
#         record['number'],
#         documents[record['number']],
#         record['distance']
#     ))
#     for i, another_record in enumerate(search_result):
#         if another_record['number'] <= record['number']:
#             continue
#
#         distance = sum((record['vector'][i] - another_record['vector'][i]) ** 2 for i in range(100))
#         if distance < 0.1:
#             print('--- {:>3} ←→ {:>3} ({}): {:0.5f}'.format(
#                 record['number'],
#                 another_record['number'],
#                 documents[another_record['number']],
#                 distance))
#     print()


### Кластеризация DBSCAN'ом

# from dbscan import dbscan
#
# points = numpy.matrix([record['vector'] for record in search_result])
# scan = dbscan(points, eps=0.316, min_points=2)
# for i, num in enumerate(scan):
#     print(num, documents[search_result[i]['number']])
