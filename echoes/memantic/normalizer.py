# coding: utf-8

import re

def normalize(text):
    return re.sub(r'[()<>1-9%№#$_=\\+*/«"»„\'“—!–”?,:.;\s-]+', ' ', text).strip().lower()
