class Config:
    DEBUG = False
    DB_TYPE = 'sqlite'
    DB_STRING = None
    APP_ID = 0
    APP_SECRET = 'fill_me'
    GROUP_SECRET = 'c00adb77b7c2a87808177460546647f8636295e96fa5036cabac0570018ee28d8068792b457231298fe0f' # TODO(sparK) Move to private config!
    GROUP_ID = 0
    UPLOADS_DEFAULT_DEST = '../uploads'
    UPLOADS_DEFAULT_URL = '/uploads/'
    SECRET_KEY = 'aFohf3ooFoh4ahm3kee7thaeb2aVaina'


class Development(Config):
    DEBUG = True
    DB_STRING = 'dev_echoes.db'


class Production(Config):
    DB_STRING = 'echoes.db'