let path = require('path');
let fs = require('fs');
let webpack = require('webpack');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let CopyWebpackPlugin = require('copy-webpack-plugin');

isProduction = process.env.NODE_ENV == 'production';

let vendors = [
    'angular',
    'angular-route',
    'angular-ui-bootstrap',
    'angular-file-upload',
    'jquery'
];

let contextPath = path.join(__dirname, 'frontend');
let config = {
    context: contextPath,
    cache: true,

    entry: {
        main: './main',
        submit: './submit',
        admin: './admin',
        vendor: vendors
    },
    output: {
        path: path.join(__dirname, 'static', '[hash]'),
        filename: '[name].bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: [
                        ["env", {
                            "targets": {
                                "browsers": ["last 2 versions", "safari >= 7"]
                            }
                        }]
                    ]
                },
                compact: true
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&minetype=application/font-woff&name=fonts/[name].[ext]"
            },
            {test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=fonts/[name].[ext]"},
            {test: /\.styl$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader!stylus-loader")},
            {test: /\.less$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")},
            {test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader")},
            {test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192'},
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader')
            },
            {test: /bootstrap-sass\/assets\/javascripts\//, loader: 'imports?jQuery=jquery'},
        ]
    },
    resolve: {
        extensions: ['', '.js', '.css'],
        modulesDirectories: ['node_modules', 'js'],
        root: [
            process.env.NODE_PATH,
            path.resolve(contextPath),
            path.resolve(path.join(__dirname, 'echoes', 'templates'))
        ]
    },
    resolveLoader: {
        root: process.env.NODE_PATH
    },
    plugins: [
        new ExtractTextPlugin("[name].css"),
        new webpack.optimize.CommonsChunkPlugin({name: 'vendor'}),
        new webpack.optimize.DedupePlugin(),
        new CopyWebpackPlugin([{from: 'img', to: '../images'}],
            {
                copyUnmodified: true
            }
        ),
        function () {
            this.plugin('done', function (stats) {
                fs.writeFileSync(
                    path.join(__dirname, 'frontend.ver'),
                    stats.hash
                );
            });
        }
    ]
};

if (isProduction) {
    config.plugins.push(new webpack.optimize.UglifyJsPlugin())
}
module.exports = config;