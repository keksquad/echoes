class SubmittedController {
    constructor($scope, $http) {
        $scope.data = false;

        $scope.loadItems = () => {
            $http.get('/api/posts').then(function (response) {
                $scope.items = response.data;
            });
        };

        $scope.setItem = (item) => {
            $scope.data = item;
        };

        $scope.approveItem = (id) => {
            $http.delete('/api/posts/'+id).then(function (response) {
                $scope.data = false;
                $scope.loadItems();
            });
        };

        $scope.deleteItem = (id) => {
            $http.delete('/api/posts/'+id).then(function (response) {
                $scope.data = false;
                $scope.loadItems();
            });
        };

        $scope.banUser = (id) => {
            $http.get('/api/banuser/'+id).then(function (response) {
                $scope.deleteItem(id);
                $scope.data = false;
                $scope.loadItems();
            });
        };
        $scope.loadItems();
    }
}

SubmittedController.$inject = ['$scope', '$http'];

export default SubmittedController;