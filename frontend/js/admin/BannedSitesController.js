class BannedSitesController {
    constructor($scope, $http) {
        $scope.loadItems = () => {
            $http.get('/api/bldomains').then(function (response) {
                $scope.items = response.data;
            });
        };

        $scope.addItem = () => {
            $http.post('/api/bldomains', { domain: $scope.domain }).then(function successCallback(response) {
                $scope.loadItems();
                $scope.domain = '';
            }, function errorCallback(response) {
                console.log('err');
            });
        };

        $scope.deleteItem = (id) => {
            $http.delete('/api/bldomains/' + id).then(function successCallback(response) {
                $scope.loadItems();
            }, function errorCallback(response) {
                console.log('err');
            });
        };

        $scope.loadItems();
    }
}

BannedSitesController.$inject = ['$scope', '$http'];

export default BannedSitesController;