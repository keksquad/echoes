class ApprovedController {
    constructor($scope, $location) {
        $scope.isRouteActive = function(route) {
            let curRoute = $location.path();
            return curRoute.match(route);
        }
    }
}

ApprovedController.$inject = ['$scope', '$location'];

export default ApprovedController;