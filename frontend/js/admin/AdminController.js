class AdminController {
    constructor($scope, $location) {
        $scope.isRouteActive = function(route) {
            let curRoute = $location.path();
            return curRoute.match(route);
        }
    }
}

AdminController.$inject = ['$scope', '$location'];

export default AdminController;