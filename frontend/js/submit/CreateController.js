class CreateController {
    constructor($scope, $http) {
        $scope.flash_info = false;
        $scope.flash_danger = false;

        $scope.files = [];
        $scope.choices = [];

        $scope.addNewChoice = function() {
            if ($scope.choices.length === 5) return;
            let newItemNo = $scope.choices.length+1;
            $scope.choices.push({'id':'link'+newItemNo});
        };

        $scope.removeChoice = function() {
            let lastItem = $scope.choices.length-1;
            $scope.choices.splice(lastItem);
        };

        $scope.send = () => {
            let data = {
                summary: $scope.summary,
                choices: $scope.choices,
                text: $scope.text,
                files: $scope.files,

                user_id: user_id,
                user_key: user_key
            };

            $http.post('/api/posts', data).then(function successCallback(response) {
                $scope.summary = '';
                $scope.text = '';
                $scope.files = [];
                $scope.choices = [];

                $scope.flash_text = 'Пост успешно отправлен!';
                $scope.flash_type = 'info';

            }, function errorCallback(response) {
                if (response.status === 403) {
                    $scope.flash_text = 'Один из указанных сайтов запрещён к публикации';
                    $scope.flash_type = 'danger';
                } else {
                    $scope.flash_text = 'Произошла ошибка при отправке запроса';
                    $scope.flash_type = 'danger';
                }

            });
        }
    }
}

CreateController.$inject = ['$scope', '$http'];

export default CreateController;