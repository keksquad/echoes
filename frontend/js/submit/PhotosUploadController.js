class PhotosUploadController {
    constructor($scope, FileUploader) {
        $scope.uploader = new FileUploader();
        $scope.uploader.url = '/upload/'+user_id+'/'+user_key;
        $scope.uploader.autoUpload = true;

        $scope.uploader.onSuccessItem = (item, response, status, headers) => {
            if(typeof response.error === "undefined") {
                // Successful upload
                $scope.files.push(response);
            } else {
                alert("Произошла ошибка при загрузке файла");
            }
        };
        let input = document.getElementById('file_upload');

        $scope.addPhoto = () => {
            input.click();
        };
    }
}

PhotosUploadController.$inject = ['$scope', 'FileUploader'];

export default PhotosUploadController;