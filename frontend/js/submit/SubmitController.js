class SubmitController {
    constructor($scope, $location) {
        $scope.isRouteActive = function(route) {
            let curRoute = $location.path();
            return curRoute.match(route);
        }
    }
}

SubmitController.$inject = ['$scope', '$location'];

export default SubmitController;