class HistoryController {
    constructor($scope, $http) {
        $scope.data = false;

        $scope.loadItems = () => {
            $http.get('/api/users/'+user_id+'/posts').then(function (response) {
                $scope.items = response.data;
            });
        };

        $scope.setItem = (item) => {
            $scope.data = item;
        };

        $scope.loadItems();
    }
}

HistoryController.$inject = ['$scope', '$http'];

export default HistoryController;