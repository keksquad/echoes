import './admin.css'

import 'angular'

let submittedUrl = require('ngtemplate!html!./js/admin/submitted.html');
let approvedUrl = require('ngtemplate!html!./js/admin/approved.html');
let bannedSitesUrl = require('ngtemplate!html!./js/admin/banned_sites.html');

import AdminController from './js/admin/AdminController';

import SubmittedController from './js/admin/SubmittedController';
import ApprovedController from './js/admin/ApprovedController';
import BannedSitesController from './js/admin/BannedSitesController';

angular.module('echoes')
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/banned-sites', {
                templateUrl: bannedSitesUrl,
                controller: 'BannedSitesController'
            })
            .when('/approved', {
                templateUrl: approvedUrl,
                controller: 'ApprovedController'
            })
            .when('/submitted', {
                templateUrl: submittedUrl,
                controller: 'SubmittedController'
            })
            .otherwise({
                redirectTo: '/submitted'
            });
    }])
    .controller('AdminController', AdminController)

    .controller('ApprovedController', ApprovedController)
    .controller('SubmittedController', SubmittedController)
    .controller('BannedSitesController', BannedSitesController);