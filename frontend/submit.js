import './submit.css'

import 'angular'

let createUrl = require('ngtemplate!html!./js/submit/create.html');
let historyUrl = require('ngtemplate!html!./js/submit/history.html');

import SubmitController from './js/submit/SubmitController';

import CreateController from './js/submit/CreateController';
import PhotosUploadController from './js/submit/PhotosUploadController';

import HistoryController from './js/submit/HistoryController';

angular.module('echoes')
    .config( ['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/history', {
                templateUrl: historyUrl,
                controller: 'HistoryController'
            })
            .when('/create', {
                templateUrl: createUrl,
                controller: 'CreateController'
            })
            .otherwise({
                redirectTo: '/create'
            });
    }])
    .controller('SubmitController', SubmitController)
    .controller('CreateController', CreateController)
    .controller('PhotosUploadController', PhotosUploadController)

    .controller('HistoryController', HistoryController);