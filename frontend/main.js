import './main.css'
import './bootstrap.scss'

import 'angular'

angular.module('echoes', ['ngRoute', 'ui.bootstrap', 'angularFileUpload']);